﻿using System;
using System.Threading.Tasks;
using eShopSolution.ViewModels.System.User;

namespace eShopSolution.AdminApp.Services
{
    public interface iUserApiClient
    {
        Task<string> Authenticate(LoginRequest request);
    }
}
