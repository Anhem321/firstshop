﻿using System;
using System.Collections.Generic;

namespace eShopSolution.ViewModels.Common
{
    public class PagedResults<T>
    {
        public List<T> Items { get; set; }
        public int TotalRecord { get; set; }
    }
}
