﻿using System;
using FluentValidation;

namespace eShopSolution.ViewModels.System.User
{
    public class LoginRequestValidator : AbstractValidator<LoginRequest>
    {
        public LoginRequestValidator()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("User Name is required");
            RuleFor(x => x.PassWord).NotEmpty().WithMessage("Passwoord is required")
                .MinimumLength(8).WithMessage("password required you must have at least 6 character");
        }
    }
}
