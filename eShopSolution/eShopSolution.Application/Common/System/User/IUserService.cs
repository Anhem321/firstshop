﻿using System;
using System.Threading.Tasks;
using eShopSolution.ViewModels.System.User;

namespace eShopSolution.Application.Common.System.User
{
    public interface IUserService
    {
        Task<string> Authenticate(LoginRequest request);
        Task<bool> Register(RegisterRequest request);
    }
}
